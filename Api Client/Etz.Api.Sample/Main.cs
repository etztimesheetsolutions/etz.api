﻿using System.Diagnostics;
using Etz.Api.Client;
using System;
using System.Windows.Forms;
using Auth0.AuthenticationApi.Models;

namespace Etz.Api.Sample
{
    public partial class Main : Form
    {
        private AccessTokenResponse _auth0Token;
        public string ApiDocumentationUrl = "https://bitbucket.org/etztimesheetsolutions/etz.api/wiki/Home";

        public Main()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                Login();

                MessageBox.Show("Login successful.", "Login", MessageBoxButtons.OK, MessageBoxIcon.Information);                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
  
        private void btnGetSalesInvoices_Click(object sender, EventArgs e)
        {
            GetSalesInvoiceBatches();
        }

        private void btnImportListOfCandidates_Click(object sender, EventArgs e)
        {
            ImportCandidates();
        }

        private void Login()
        {
            _auth0Token = null;
            var auth0 = new Auth0(txtEmailAddress.Text, txtPassword.Text);
            _auth0Token = auth0.Login();
        }

        private void ValidateHasLoggedIntoAuth0()
        {
            if (_auth0Token == null)
                throw new Exception("Oops...you must login in first!");
        }

        private void GetSalesInvoiceBatches()
        {
            try
            {
                lbSalesInvoiceBatches.Items.Clear();

                ValidateHasLoggedIntoAuth0();

                var operation = new SalesInvoiceBatch(_auth0Token);
                var salesInvoiceBatches = operation.GetList();


                foreach (var batch in salesInvoiceBatches)
                {
                    lbSalesInvoiceBatches.Items.Add(string.Format("BatchId: {0} - Total Amount: {1}", batch.BatchId, batch.TotalAmt));
                }

                MessageBox.Show("Operation successful.", "Get Sales Invoice Batches", MessageBoxButtons.OK, MessageBoxIcon.Information);    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Get Sales Invoices Batches", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ImportCandidates()
        {
            try
            {
                ClearImportResultsList();

                ValidateHasLoggedIntoAuth0();

                var operation = new Import(_auth0Token);
                var importListResponse = operation.ImportCandidateList(@"TestFiles\CandidateMappings.csv");

                UpdateImportResultsList(importListResponse);
                
                
                MessageBox.Show("Operation successful.", "Import Candidates", MessageBoxButtons.OK, MessageBoxIcon.Information);    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Import Candidates", MessageBoxButtons.OK, MessageBoxIcon.Error);                
            }
        }
        
        private void llApiDocumentation_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(ApiDocumentationUrl);
        }

        private void ClearImportResultsList()
        {
            lbImportResults.Items.Clear();
        }
   
        private void UpdateImportResultsList(Client.Models.ImportListResponse importListResponse)
        {
            foreach (var data in importListResponse.ResponseData)
                lbImportResults.Items.Add(string.Format("Row #: {0} - BatchUid: {1} - ItemId {2}", data.RowNumber, data.ImportBatchUid, data.ItemId));
        }
    }
}
