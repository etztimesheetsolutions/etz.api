﻿namespace Etz.Api.Sample
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEmailAddress = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnGetSalesInvoiceBatches = new System.Windows.Forms.Button();
            this.lbSalesInvoiceBatches = new System.Windows.Forms.ListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnImportListOfCandidates = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbImportResults = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.llApiDocumentation = new System.Windows.Forms.LinkLabel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkCyan;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(463, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome to the Etz API sample application";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(491, 91);
            this.label2.TabIndex = 1;
            this.label2.Text = resources.GetString("label2.Text");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(301, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Enter your Auth0 identity credentials";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Email address:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(279, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Password:";
            // 
            // txtEmailAddress
            // 
            this.txtEmailAddress.Location = new System.Drawing.Point(98, 101);
            this.txtEmailAddress.Name = "txtEmailAddress";
            this.txtEmailAddress.Size = new System.Drawing.Size(164, 20);
            this.txtEmailAddress.TabIndex = 5;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(341, 101);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(164, 20);
            this.txtPassword.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(467, 39);
            this.label6.TabIndex = 7;
            this.label6.Text = "Enter your Auth0 credentials that your previously registered with via Etz web sit" +
    "e.\r\n\r\nNote - The Etz User associated with this identity must have the \'External " +
    "Data Access\' role added.\r\n";
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(511, 99);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 8;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(15, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(306, 20);
            this.label7.TabIndex = 9;
            this.label7.Text = "Sample 1 - Get sales invoice batches";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(221, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "This will return a list of all your Sales Invoices.\r\n";
            // 
            // btnGetSalesInvoiceBatches
            // 
            this.btnGetSalesInvoiceBatches.Location = new System.Drawing.Point(17, 74);
            this.btnGetSalesInvoiceBatches.Name = "btnGetSalesInvoiceBatches";
            this.btnGetSalesInvoiceBatches.Size = new System.Drawing.Size(151, 23);
            this.btnGetSalesInvoiceBatches.TabIndex = 11;
            this.btnGetSalesInvoiceBatches.Text = "Get Sales Invoices Batches";
            this.btnGetSalesInvoiceBatches.UseVisualStyleBackColor = true;
            this.btnGetSalesInvoiceBatches.Click += new System.EventHandler(this.btnGetSalesInvoices_Click);
            // 
            // lbSalesInvoiceBatches
            // 
            this.lbSalesInvoiceBatches.FormattingEnabled = true;
            this.lbSalesInvoiceBatches.Location = new System.Drawing.Point(174, 76);
            this.lbSalesInvoiceBatches.Name = "lbSalesInvoiceBatches";
            this.lbSalesInvoiceBatches.Size = new System.Drawing.Size(236, 134);
            this.lbSalesInvoiceBatches.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 49);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(296, 39);
            this.label9.TabIndex = 14;
            this.label9.Text = "This will upload a CSV file of candidates.\r\n\r\nThe file is located in the \'TestFil" +
    "es\\CandidateMapppings.csv\'.\r\n";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(17, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(229, 20);
            this.label10.TabIndex = 13;
            this.label10.Text = "Sample 2 - Upload a list file";
            // 
            // btnImportListOfCandidates
            // 
            this.btnImportListOfCandidates.Location = new System.Drawing.Point(21, 101);
            this.btnImportListOfCandidates.Name = "btnImportListOfCandidates";
            this.btnImportListOfCandidates.Size = new System.Drawing.Size(153, 23);
            this.btnImportListOfCandidates.TabIndex = 15;
            this.btnImportListOfCandidates.Text = "Import a list of candidates";
            this.btnImportListOfCandidates.UseVisualStyleBackColor = true;
            this.btnImportListOfCandidates.Click += new System.EventHandler(this.btnImportListOfCandidates_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.btnGetSalesInvoiceBatches);
            this.groupBox1.Controls.Add(this.lbSalesInvoiceBatches);
            this.groupBox1.Location = new System.Drawing.Point(18, 351);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(428, 230);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Get Sales Invoice Batches";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbImportResults);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.btnImportListOfCandidates);
            this.groupBox2.Location = new System.Drawing.Point(452, 351);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(428, 230);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Upload a Candidates List";
            // 
            // lbImportResults
            // 
            this.lbImportResults.FormattingEnabled = true;
            this.lbImportResults.Location = new System.Drawing.Point(21, 129);
            this.lbImportResults.Name = "lbImportResults";
            this.lbImportResults.Size = new System.Drawing.Size(394, 82);
            this.lbImportResults.TabIndex = 16;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.btnLogin);
            this.groupBox3.Controls.Add(this.txtEmailAddress);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtPassword);
            this.groupBox3.Location = new System.Drawing.Point(18, 182);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(862, 153);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Enter your Auth0 Identity";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Etz.Api.Sample.Properties.Resources.ETZ_LOGO_RGB_72dpi;
            this.pictureBox1.Location = new System.Drawing.Point(763, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(117, 117);
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // llApiDocumentation
            // 
            this.llApiDocumentation.AutoSize = true;
            this.llApiDocumentation.Location = new System.Drawing.Point(28, 143);
            this.llApiDocumentation.Name = "llApiDocumentation";
            this.llApiDocumentation.Size = new System.Drawing.Size(117, 13);
            this.llApiDocumentation.TabIndex = 20;
            this.llApiDocumentation.TabStop = true;
            this.llApiDocumentation.Text = "Etz API Documentation";
            this.llApiDocumentation.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llApiDocumentation_LinkClicked);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(896, 599);
            this.Controls.Add(this.llApiDocumentation);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Etz API Sample Application";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtEmailAddress;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnGetSalesInvoiceBatches;
        private System.Windows.Forms.ListBox lbSalesInvoiceBatches;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnImportListOfCandidates;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel llApiDocumentation;
        private System.Windows.Forms.ListBox lbImportResults;
    }
}

