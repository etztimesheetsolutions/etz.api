﻿using Auth0.AuthenticationApi;
using Auth0.AuthenticationApi.Models;

namespace Etz.Api.Sample
{
    public class Auth0
    {
        private readonly string _emailAddress;
        private readonly string _password;

        public Auth0(string emailAddress, string password)
        {
            _emailAddress = emailAddress;
            _password = password;
        }

        public AccessTokenResponse Login()
        {
            var domain = System.Configuration.ConfigurationManager.AppSettings["Auth0Domain"];
            var client = System.Configuration.ConfigurationManager.AppSettings["Auth0ClientID"];
            var audience = System.Configuration.ConfigurationManager.AppSettings["Auth0Audience"];

            // Sign into Auth0
            var auth0Client = new AuthenticationApiClient(domain);
            var requestOptions = new ResourceOwnerTokenRequest
            {
                ClientId = client,
                Username = _emailAddress,
                Password = _password,
                Audience = audience
            };

            var result = auth0Client.GetTokenAsync(requestOptions).Result;
            return result;
        }
    }
}
