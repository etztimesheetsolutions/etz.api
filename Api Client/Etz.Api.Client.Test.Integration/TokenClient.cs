﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using IdentityModel.Client;
using System.Configuration;

namespace Etz.Api.Client.Test.Integration
{
    public class TokenClient : IDisposable
    {
        private HttpClient tokenClient { get; set; }
        private DiscoveryResponse disco { get; set; }

        public TokenClient()
        {
            SetUpClient();
        }

        private void SetUpClient()
        {
            tokenClient = new HttpClient();
            disco = tokenClient.GetDiscoveryDocumentAsync(ConfigurationManager.AppSettings["Goowid:Domain"]).Result;

            if (disco.IsError) throw new Exception(disco.Error);

        }

        public void Dispose()
        {
            tokenClient?.Dispose();
        }

        public async Task<TokenResponse> AuthenticateUser(string username, string password)
        {
            if (tokenClient == null)
                tokenClient = new HttpClient();

            var response = await tokenClient.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,

                ClientId = ConfigurationManager.AppSettings["Goowid:ApiClientId"],
                ClientSecret = ConfigurationManager.AppSettings["Goowid:ApiClientSecret"],

                UserName = username,
                Password = password
            });

            return response;
        }
    }
}

  
