﻿using NUnit;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using IdentityModel.Client;
using NUnit.Framework;

namespace Etz.Api.Client.Test.Integration
{
    public class ClientTests
    {
        private readonly string WebServiceBaseAddress = ConfigurationManager.AppSettings["WebServiceBaseAddress"];
        private HttpClient _client;
        private TokenClient _tokenClient;
        private string _username = ConfigurationManager.AppSettings["Username"];
        private string _password = ConfigurationManager.AppSettings["Password"];
        private TokenResponse _token;

        [OneTimeSetUp]
        public void FixtureSetup()
        {
            _tokenClient = new TokenClient();
            _token = _tokenClient.AuthenticateUser(_username, _password).Result;


        }

        [SetUp]
        public void SetupBeforeEachTest()
        {
            System.Threading.Thread.Sleep(1500);
        }

        [Test]
        public void AssignmentEndAssignment_Should()
        {
            var operation = new Assignment(_token);
            operation.EndAssignment(44649, DateTime.Now, "1");
        }

        [Test]
        public void  BullhornBulkUpdatePlacement_Should()
        {
            var operation = new Bullhorn(_token);
            operation.BulkUpdatePlacement(new[] { 1, 2, 3 });
        }

        [Test]
        public void ImportAssignmentList_Should()
        {
            var operation = new Import(_token);
            var importListResponse = operation.ImportAssignmentList(@"TestFiles\AssignmentMappings.csv");

            Assert.AreEqual(1, importListResponse.ResponseData[0].RowNumber);
            Assert.IsTrue(importListResponse.ResponseData[0].ImportBatchUid != Guid.Empty);
            Assert.IsTrue(importListResponse.ResponseData[0].ItemId != Guid.Empty);
        }

        [Test]
        public void ImportAssignmentListWithBytes_Should()
        {
            var operation = new Import(_token);
            operation.ImportAssignmentList(@"TestFiles\AssignmentCommissionMappings.csv", new byte[] { 0 });
        }

        [Test]
        public void ImportAssignmentCommissionList_Should()
        {
            var operation = new Import(_token);
            var importListResponse = operation.ImportAssignmentCommissionList(@"TestFiles\AssignmentCommissionMappings.csv");

            Assert.AreEqual(1, importListResponse.ResponseData[0].RowNumber);
            Assert.IsTrue(importListResponse.ResponseData[0].ImportBatchUid != Guid.Empty);
            Assert.IsTrue(importListResponse.ResponseData[0].ItemId != Guid.Empty);
        }

        [Test]
        public void ImportAssignmentRateList_Should()
        {
            var operation = new Import(_token);
            var importListResponse = operation.ImportAssignmentRateList(@"TestFiles\AssignmentRateMappings.csv");

            Assert.AreEqual(1, importListResponse.ResponseData[0].RowNumber);
            Assert.IsTrue(importListResponse.ResponseData[0].ImportBatchUid != Guid.Empty);
            Assert.IsTrue(importListResponse.ResponseData[0].ItemId != Guid.Empty);
        }

        [Test]
        public void ImportCandidateList_Should()
        {
            var operation = new Import(_token);
            var importListResponse = operation.ImportCandidateList(@"TestFiles\CandidateMappings.csv");

            Assert.AreEqual(1, importListResponse.ResponseData[0].RowNumber);
            Assert.IsTrue(importListResponse.ResponseData[0].ImportBatchUid != Guid.Empty);
            Assert.IsTrue(importListResponse.ResponseData[0].ItemId != Guid.Empty);
        }

        [Test]
        public void ImportClientAuthoriserList_Should()
        {
            var operation = new Import(_token);
            var importListResponse = operation.ImportClientAuthoriserList(@"TestFiles\ClientAuthoriserMappings.csv");

            Assert.AreEqual(1, importListResponse.ResponseData[0].RowNumber);
            Assert.IsTrue(importListResponse.ResponseData[0].ImportBatchUid != Guid.Empty);
            Assert.IsTrue(importListResponse.ResponseData[0].ItemId != Guid.Empty);
        }

        [Test]
        public void ImportClientList_Should()
        {
            var operation = new Import(_token);
            var importListResponse = operation.ImportClientList(@"TestFiles\ClientMappings.csv");

            Assert.AreEqual(1, importListResponse.ResponseData[0].RowNumber);
            Assert.IsTrue(importListResponse.ResponseData[0].ImportBatchUid != Guid.Empty);
            Assert.IsTrue(importListResponse.ResponseData[0].ItemId != Guid.Empty);
        }

        [Test]
        public void ImportConsultantCommissionSplitList_Should()
        {
            var operation = new Import(_token);
            var importListResponse = operation.ImportConsultantCommissionSplitList(@"TestFiles\ConsultantCommissionSplitMappings.csv");

            Assert.AreEqual(1, importListResponse.ResponseData[0].RowNumber);
            Assert.IsTrue(importListResponse.ResponseData[0].ImportBatchUid != Guid.Empty);
            Assert.IsTrue(importListResponse.ResponseData[0].ItemId != Guid.Empty);
        }

        [Test]
        public void ImportConsultantList_Should()
        {
            var operation = new Import(_token);
            var importListResponse = operation.ImportConsultantList(@"TestFiles\ConsultantMappings.csv");

            Assert.AreEqual(1, importListResponse.ResponseData[0].RowNumber);
            Assert.IsTrue(importListResponse.ResponseData[0].ImportBatchUid != Guid.Empty);
            Assert.IsTrue(importListResponse.ResponseData[0].ItemId != Guid.Empty);
        }

        [Test]
        public void ImportSupplierList_Should()
        {
            var operation = new Import(_token);
            var importListResponse = operation.ImportSupplierList(@"TestFiles\SupplierMappings.csv");

            Assert.AreEqual(1, importListResponse.ResponseData[0].RowNumber);
            Assert.IsTrue(importListResponse.ResponseData[0].ImportBatchUid != Guid.Empty);
            Assert.IsTrue(importListResponse.ResponseData[0].ItemId != Guid.Empty);
        }

        [Test]
        public void ImportPermanentAssignmentList_Should()
        {
            var operation = new Import(_token);
            var importListResponse = operation.ImportPermanentAssignmentList(@"TestFiles\PermanentAssignmentMappings.csv");

            Assert.AreEqual(1, importListResponse.ResponseData[0].RowNumber);
            Assert.IsTrue(importListResponse.ResponseData[0].ImportBatchUid != Guid.Empty);
            Assert.IsTrue(importListResponse.ResponseData[0].ItemId != Guid.Empty);
        }

        [Test]
        public void ImportTimesheetList_Should()
        {
            var operation = new Import(_token);
            var importListResponse = operation.ImportTimesheetList(@"TestFiles\TimesheetMappings.csv");

            Assert.AreEqual(1, importListResponse.ResponseData[0].RowNumber);
            Assert.IsTrue(importListResponse.ResponseData[0].ImportBatchUid != Guid.Empty);
            Assert.IsTrue(importListResponse.ResponseData[0].ItemId != Guid.Empty);
        }

        [Test]
        public void ImportUpdateExistingTimesheetList_Should()
        {
            var operation = new Import(_token);
            var importListResponse = operation.ImportUpdateExistingTimesheetList(@"Testfiles\UpdatedTimesheetMappings.csv");

            Assert.AreEqual(1, importListResponse.ResponseData[0].RowNumber);
            Assert.IsTrue(importListResponse.ResponseData[0].ImportBatchUid != Guid.Empty);
            Assert.IsTrue(importListResponse.ResponseData[0].ItemId != Guid.Empty);
        }

        [Test]
        public void PayrollBatchGetList_Should()
        {
            var operation = new PayrollBatch(_token);
            operation.GetList();
        }

        [Test]
        public void PayrollBatchGetUnprocessedList_Should()
        {
            var operation = new PayrollBatch(_token);
            operation.GetUnprocessedList();
        }

        [Test]
        public void PurchaseInvoiceGetList_Should()
        {
            var operation = new PurchaseInvoice(_token);
            operation.GetList(79488);
        }

        [Test]
        public void PurchaseInvoiceBatchGetList_Should()
        {
            var operation = new PurchaseInvoiceBatch(_token);
            operation.GetList();
        }

        [Test]
        public void PurchaseInvoiceBatchGetUnprocessedList_Should()
        {
            var operation = new PurchaseInvoiceBatch(_token);
            operation.GetUnprocessedList();
        }

        [Test]
        public void SalesInvoiceGetList_Should()
        {
            var operation = new SalesInvoice(_token);
            operation.GetList(155925);
        }

        [Test]
        public void SalesInvoiceBatchGetList_Should()
        {
            var operation = new SalesInvoiceBatch(_token);
            operation.GetList();
        }

        [Test]
        public void SalesInvoiceBatchGetUnprocessedList_Should()
        {
            var operation = new SalesInvoiceBatch(_token);
            operation.GetUnprocessedList();
        }

        [Test]
        public void TimesheetPayrollGetList_Should()
        {
            var operation = new TimesheetPayroll(_token);
            operation.GetList(157425);
        }

        [Test]
        public void ImportStatusGetItemStatus_Should()
        {
            var operation = new ImportStatus(_token);
            operation.GetItemStatus(Guid.NewGuid());
        }

        [Test]
        public void ImportStatusGetBatchStatus_Should()
        {
            var operation = new ImportStatus(_token);
            operation.GetBatchStatus(Guid.NewGuid());
        }
    }
}
