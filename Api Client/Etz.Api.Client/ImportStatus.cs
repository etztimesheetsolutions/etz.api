﻿using System;
using IdentityModel.Client;
using Etz.Api.Client.Utilities;

namespace Etz.Api.Client
{
    public class ImportStatus
    {
        private readonly TokenResponse _token;

        public ImportStatus(TokenResponse token)
        {
            _token = token;
        }

        public Models.ImportStatus GetItemStatus(Guid itemId)
        {
            var param = string.Format("itemid={0}", itemId);

            var requestBuilder = new RequestBuilder(_token);
            return requestBuilder.Get<Models.ImportStatus>("ImportStatus/GetItemStatus", param);
        }

        public Models.ImportStatus GetBatchStatus(Guid importBatchUid)
        {
            var param = string.Format("importBatchUid={0}", importBatchUid);

            var requestBuilder = new RequestBuilder(_token);
            return requestBuilder.Get<Models.ImportStatus>("ImportStatus/GetBatchStatus", param);
        }
    }
}
