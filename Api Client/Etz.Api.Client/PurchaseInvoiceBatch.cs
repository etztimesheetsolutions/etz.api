﻿using System.Collections.Generic;
using IdentityModel.Client;
using Etz.Api.Client.Utilities;

namespace Etz.Api.Client
{
    public class PurchaseInvoiceBatch
    {
        private readonly TokenResponse _token;

        public PurchaseInvoiceBatch(TokenResponse token)
        {
            _token = token;
        }

        public IEnumerable<Models.PurchaseInvoiceBatch> GetList()
        {
            const string method = "PurchaseInvoiceBatch/GetList";

            var requestBuilder = new RequestBuilder(_token);
            return requestBuilder.Get<IEnumerable<Models.PurchaseInvoiceBatch>>(method);
        }

        public IEnumerable<Models.PurchaseInvoiceBatch> GetUnprocessedList()
        {
            const string method = "PurchaseInvoiceBatch/GetUnprocessedList";

            var requestBuilder = new RequestBuilder(_token);
            return requestBuilder.Get<IEnumerable<Models.PurchaseInvoiceBatch>>(method);
        }
    }
}
