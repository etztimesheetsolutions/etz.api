﻿
using Etz.Api.Client.Models;
using Etz.Api.Client.Utilities;
using IdentityModel.Client;
namespace Etz.Api.Client
{
    public class ImportBase
    {
        private readonly TokenResponse _token;

        public ImportBase(TokenResponse token){
            _token = token;
        }

        public ImportListResponse ImportList(string fileName, string methodName) 
        {
            var bytes = File.ReadFileContentToBytes(fileName);

            return ImportList(fileName, bytes, methodName);
        }

        public ImportListResponse ImportList(string fileName, byte[] bytes, string methodName)
        {
            var listRequestData = File.ConvertBytesToBase64String(bytes);

            var requestData = new ImportListRequest { FileName = fileName, ListRequestData = listRequestData };

            var requestBuilder = new RequestBuilder(_token);
            return requestBuilder.Post<ImportListResponse, ImportListRequest>(methodName, requestData);
        }
    }
}
