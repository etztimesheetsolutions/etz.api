﻿
namespace Etz.Api.Client.Models
{
    public class ImportStatus
    {
        public bool Imported { get; set; }
        public string Comment { get; set; }
    }
}
