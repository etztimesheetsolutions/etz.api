﻿using System;

namespace Etz.Api.Client.Models
{
    public class EndAssignmentRequest
    {
        public int AssignmentId { get; set; }
        public DateTime EndDate { get; set; }
        public string AgencyRef { get; set; }
    }
}
