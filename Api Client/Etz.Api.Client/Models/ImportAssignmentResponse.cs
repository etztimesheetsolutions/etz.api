﻿namespace Etz.Api.Client.Models
{
    public class ImportAssignmentResponse
    {
        public ImportAssignmentData AssignmentData { get; set; }
    }
}
