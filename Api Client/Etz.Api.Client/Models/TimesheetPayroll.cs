﻿using System;

namespace Etz.Api.Client.Models
{
    public class TimesheetPayroll
    {
        /// <summary>
        /// EmployeeNo
        /// </summary>
        public string EmployeeNo { get; set; }
        /// <summary>
        /// PayCode
        /// </summary>
        public string PayCode { get; set; }
        /// <summary>
        /// Units
        /// </summary>
        public decimal Units { get; set; }
        /// <summary>
        /// Rate
        /// </summary>
        public decimal Rate { get; set; }
        /// <summary>
        /// Sector
        /// </summary>
        public string Sector { get; set; }
        /// <summary>
        /// EndDate
        /// </summary>
        public DateTime EndDate { get; set; }   
    }
}
