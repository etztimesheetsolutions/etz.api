﻿
namespace Etz.Api.Client.Models
{
    public class BulkUpdatePlacementRequest
    {
        public string PlacementList { get; set; }
    }
}
