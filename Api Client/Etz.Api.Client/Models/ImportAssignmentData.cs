﻿
namespace Etz.Api.Client.Models
{
    public class ImportAssignmentData
    {
        /// <summary>
        /// Assignment is a Base64 encoded JSon string representing your Assignment details.
        /// </summary>
        public string Assignment { get; set; }

        /// <summary>
        /// Candidate is a Base64 encoded JSon string representing your Candidate details.
        /// </summary>
        public string Candidate { get; set; }

        /// <summary>
        /// Client is a Base64 encoded JSon string representing your Client details.
        /// </summary>
        public string Client { get; set; }

        /// <summary>
        /// ClientAuthoriser is a Base64 encoded JSon string representing your Client Authoriser details.
        /// </summary>
        public string ClientAuthoriser { get; set; }

        /// <summary>
        /// Consultant is a Base64 encoded JSon string representing your Consultant details.
        /// </summary>
        public string Consultant { get; set; }

        /// <summary>
        /// ConsultantCommissionSplit is a Base64 encoded JSon string representing your Consultant Commission Splits details.
        /// </summary>
        public string ConsultantCommissionSplit { get; set; }

        /// <summary>
        /// AssignmentRate is a Base64 encoded JSon string representing your Assignment Rates details.
        /// </summary>
        public string AssignmentRate { get; set; }

        /// <summary>
        /// AssignmentRate is a Base64 encoded JSon string representing your Assignment Commission details.
        /// </summary>
        public string AssignmentCommission { get; set; }
    }
}
