﻿using System;

namespace Etz.Api.Client.Models
{
    public class PayrollBatch
    {
        public int BatchId { get; set; }
        public DateTime DateTimeProcessed { get; set; }
        public int PayRunId { get; set; }
        public int AgencyId { get; set; }
        public int PayRunType { get; set; }
        public decimal TotalAmt { get; set; }
        public decimal AgencyTotalAmt { get; set; }
        public decimal TotalVat { get; set; }
        public decimal AgencyTotalVat { get; set; }
        public string FromCurrencyCode { get; set; }
        public string ToCurrencyCode { get; set; }
        public DateTime DateProcessed { get; set; }
        public int PayRunStatus { get; set; }
        public string StatusNote { get; set; }
        public int AuthorisedById { get; set; }
        public int ItemNo { get; set; }
        public DateTime ValueDate { get; set; }
        public int DocumentId { get; set; }
        public int CurrencyId { get; set; }
        public bool IsDocumentDirty { get; set; }
        public bool IsPayRunLocked { get; set; }
        public bool IsRecalculating { get; set; }
        public string PayRunIdAndDisplayDateTime { get; set; }
        public string Label { get; set; }
        public string Notes { get; set; }
    }
}
