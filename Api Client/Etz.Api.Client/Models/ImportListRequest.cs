﻿namespace Etz.Api.Client.Models
{
    public class ImportListRequest
    {
        public string FileName { get; set; }
        public string ListRequestData { get; set; }
    }
}
