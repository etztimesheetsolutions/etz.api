﻿using System;

namespace Etz.Api.Client.Models
{
    public class PurchaseInvoiceBatch
    {
        /// <summary>
        /// Invoice No
        /// </summary>
        public int InvoiceNo { get; set; }
        /// <summary>
        /// AccountCode
        /// </summary>
        public string AccountCode { get; set; }
        /// <summary>
        /// AccountingInterface
        /// </summary>
        public string AccountingInterface { get; set; }
        /// <summary>
        /// InvoiceDate
        /// </summary>
        public DateTime InvoiceDate { get; set; }
        /// <summary>
        /// NetAmount
        /// </summary>
        public decimal NetAmount { get; set; }
        /// <summary>
        /// TaxAmount
        /// </summary>
        public decimal TaxAmount { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Consultant
        /// </summary>
        public string Consultant { get; set; }
        /// <summary>
        /// Sector
        /// </summary>
        public string Sector { get; set; }
    }
}
