﻿using System;

namespace Etz.Api.Client.Models
{
    public class ImportEntity
    {
        public int RowNumber { get; set; }
        public Guid ItemId { get; set; }
        public Guid ImportBatchUid { get; set; }
    }
}
