﻿using System;

namespace Etz.Api.Client.Models
{
    public class PurchaseInvoice
    {
        /// <summary>
        /// PayInvoiceID
        /// </summary>
        public int PayInvoiceId { get; set; }

        /// <summary>
        /// AgencyId
        /// </summary>
        public int AgencyId { get; set; }

        /// <summary>
        /// LtdCompanyId
        /// </summary>
        public int LtdCompanyId { get; set; }

        /// <summary>
        /// AssignmentId
        /// </summary>
        public int AssignmentId { get; set; }

        /// <summary>
        /// RowVersion
        /// </summary>
        public byte[] RowVersion { get; set; }

        /// <summary>
        /// DateRaised
        /// </summary>
        public DateTime DateRaised { get; set; }

        /// <summary>
        /// Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// VatAmt
        /// </summary>
        public decimal VatAmt { get; set; }

        /// <summary>
        /// CurrencyId
        /// </summary>
        public int CurrencyId { get; set; }

        /// <summary>
        /// DocumentId
        /// </summary>
        public int DocumentId { get; set; }

        /// <summary>
        /// BaseExchangeRateId
        /// </summary>
        public int BaseExchangeRateId { get; set; }

        /// <summary>
        /// PayRunId
        /// </summary>
        public int PayRunId { get; set; }

        /// <summary>
        /// DatePaid
        /// </summary>
        public DateTime DatePaid { get; set; }

        /// <summary>
        /// AgencyRef
        /// </summary>
        public int AgencyRef { get; set; }

        /// <summary>
        /// LtdCompanyRef
        /// </summary>
        public string LtdCompanyRef { get; set; }

        /// <summary>
        /// IsSelfBill
        /// </summary>
        public bool IsSelfBill { get; set; }

        /// <summary>
        /// PaymentMethodId
        /// </summary>
        public int PaymentMethodId { get; set; }

        /// <summary>
        /// DeliveryDocumentId
        /// </summary>
        public int DeliveryDocumentId { get; set; }

        /// <summary>
        /// IsDelivered
        /// </summary>
        public bool IsDelivered { get; set; }
    }
}
