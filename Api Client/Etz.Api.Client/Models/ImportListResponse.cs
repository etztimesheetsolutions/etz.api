﻿using System.Collections.Generic;

namespace Etz.Api.Client.Models
{
    public class ImportListResponse
    {
        public List<ImportEntity> ResponseData { get; set; }
    }
}
