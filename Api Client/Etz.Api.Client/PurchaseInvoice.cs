﻿using System.Collections.Generic;
using IdentityModel.Client;
using Etz.Api.Client.Utilities;

namespace Etz.Api.Client
{
    public class PurchaseInvoice
    {
        private readonly TokenResponse _token;

        public PurchaseInvoice(TokenResponse token)
        {
            _token = token;
        }

        public IEnumerable<Models.PurchaseInvoice> GetList(int batchId)
        {
            var methodAndParam = string.Format("PurchaseInvoice/GetList?BatchId={0}", batchId);

            var requestBuilder = new RequestBuilder(_token);
            return requestBuilder.Get<IEnumerable<Models.PurchaseInvoice>>(methodAndParam);
        }
    }
}
