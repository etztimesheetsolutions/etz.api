﻿
using Etz.Api.Client.Models;
using IdentityModel.Client;

namespace Etz.Api.Client
{
    public class Import : ImportBase
    {
        public Import(TokenResponse token) : base(token)
        {
        }

        public ImportListResponse ImportAssignmentList(string fileName)
        {
            return ImportList(fileName, "Import/AssignmentList");
        }

        public ImportListResponse ImportAssignmentList(string fileName, byte[] bytes)
        {
            return ImportList(fileName, bytes, "Import/AssignmentList");
        }

        public ImportListResponse ImportAssignmentCommissionList(string fileName)
        {
            return ImportList(fileName, "Import/AssignmentCommissionList");
        }

        public ImportListResponse ImportAssignmentCommissionList(string fileName, byte[] bytes)
        {
            return ImportList(fileName, bytes, "Import/AssignmentCommissionList");
        }

        public ImportListResponse ImportAssignmentRateList(string fileName)
        {
            return ImportList(fileName, "Import/AssignmentRateList");
        }

        public ImportListResponse ImportAssignmentRateList(string fileName, byte[] bytes)
        {
            return ImportList(fileName, bytes, "Import/AssignmentRateList");
        }

        public ImportListResponse ImportCandidateList(string fileName)
        {
            return ImportList(fileName, "Import/CandidateList");
        }

        public ImportListResponse ImportCandidateList(string fileName, byte[] bytes)
        {
            return ImportList(fileName, bytes, "Import/CandidateList");
        }

        public ImportListResponse ImportClientList(string fileName)
        {
            return ImportList(fileName, "Import/ClientList");
        }

        public ImportListResponse ImportClientList(string fileName, byte[] bytes)
        {
            return ImportList(fileName, bytes, "Import/ClientList");
        }

        public ImportListResponse ImportClientAuthoriserList(string fileName)
        {
            return ImportList(fileName, "Import/ClientAuthoriserList");
        }

        public ImportListResponse ImportClientAuthoriserList(string fileName, byte[] bytes)
        {
            return ImportList(fileName, bytes, "Import/ClientAuthoriserList");
        }

        public ImportListResponse ImportConsultantList(string fileName)
        {
            return ImportList(fileName, "Import/ConsultantList");
        }

        public ImportListResponse ImportConsultantList(string fileName, byte[] bytes)
        {
            return ImportList(fileName, bytes, "Import/ConsultantList");
        }

        public ImportListResponse ImportConsultantCommissionSplitList(string fileName)
        {
            return ImportList(fileName, "Import/ConsultantCommissionSplitList");
        }

        public ImportListResponse ImportConsultantCommissionSplitList(string fileName, byte[] bytes)
        {
            return ImportList(fileName, bytes, "Import/ConsultantCommissionSplitList");
        }

        public ImportListResponse ImportSupplierList(string fileName)
        {
            return ImportList(fileName, "Import/SupplierList");
        }

        public ImportListResponse ImportSupplierList(string fileName, byte[] bytes)
        {
            return ImportList(fileName, bytes, "Import/SupplierList");
        }

        public ImportListResponse ImportPermanentAssignmentList(string fileName)
        {
            return ImportList(fileName, "Import/PermanentAssignmentList");
        }

        public ImportListResponse ImportPermanentAssignmentList(string fileName, byte[] bytes)
        {
            return ImportList(fileName, bytes, "Import/PermanantAssignmentList");
        }

        public ImportListResponse ImportTimesheetList(string fileName)
        {
            return ImportList(fileName, "Import/TimesheetList");
        }

        public ImportListResponse ImportTimesheetList(string fileName, byte[] bytes)
        {
            return ImportList(fileName, bytes, "Import/TimesheetList");
        }

        public ImportListResponse ImportUpdateExistingTimesheetList(string fileName)
        {
            return ImportList(fileName, "Import/UpdateExistingTimesheetList");
        }

        public ImportListResponse ImportUpdateExistingTimesheetList(string fileName, byte[] bytes)
        {
            return ImportList(fileName, bytes, "Import/UpdateExistingTimesheetList");
        }
    }
}
