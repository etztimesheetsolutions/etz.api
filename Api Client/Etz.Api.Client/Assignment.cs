﻿using Etz.Api.Client.Models;
using Etz.Api.Client.Utilities;
using System;
using IdentityModel.Client;

namespace Etz.Api.Client
{
    public class Assignment
    {
        private readonly TokenResponse _token;

        public Assignment(TokenResponse token)
        {
            _token = token;
        }

        public void EndAssignment(int assignmentId, DateTime endDate, string agencyRef)
        {
            var requestData = new EndAssignmentRequest { AssignmentId = assignmentId, EndDate = endDate, AgencyRef = agencyRef };

            var requestBuilder = new RequestBuilder(_token);
            requestBuilder.Post("Assignment/EndAssignment", requestData);
        }
    }
}
