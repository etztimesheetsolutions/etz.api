﻿using System.Collections.Generic;
using IdentityModel.Client;
using Etz.Api.Client.Utilities;

namespace Etz.Api.Client
{
    public class PayrollBatch
    {
        private readonly TokenResponse _token;

        public PayrollBatch(TokenResponse token)
        {
            _token = token;
        }

        public IEnumerable<Models.PayrollBatch> GetList()
        {
            var requestBuilder = new RequestBuilder(_token);
            return requestBuilder.Get<IEnumerable<Models.PayrollBatch>>("PayrollBatch/GetList");
        }

        public IEnumerable<Models.PayrollBatch> GetUnprocessedList()
        {
            var requestBuilder = new RequestBuilder(_token);
            return requestBuilder.Get<IEnumerable<Models.PayrollBatch>>("PayrollBatch/GetUnprocessedList");
        }
    }
}
