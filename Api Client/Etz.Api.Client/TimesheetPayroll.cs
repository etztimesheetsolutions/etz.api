﻿using System.Collections.Generic;
using IdentityModel.Client;
using Etz.Api.Client.Utilities;

namespace Etz.Api.Client
{
    public class TimesheetPayroll
    {
        private readonly TokenResponse _token;

        public TimesheetPayroll(TokenResponse token)
        {
            _token = token;
        }

        public IEnumerable<Models.TimesheetPayroll> GetList(int batchId)
        {
            var methodAndParam = string.Format("TimesheetPayroll/GetList?BatchId={0}", batchId);

            var requestBuilder = new RequestBuilder(_token);
            return requestBuilder.Get<IEnumerable<Models.TimesheetPayroll>>(methodAndParam);
        }
    }
}
