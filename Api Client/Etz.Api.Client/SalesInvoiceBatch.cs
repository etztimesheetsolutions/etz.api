﻿using System.Collections.Generic;
using IdentityModel.Client;
using Etz.Api.Client.Utilities;

namespace Etz.Api.Client
{
    public class SalesInvoiceBatch
    {
        private readonly TokenResponse _token;

        public SalesInvoiceBatch(TokenResponse token)
        {
            _token = token;
        }

        public IEnumerable<Models.SalesInvoiceBatch> GetList()
        {
            var requestBuilder = new RequestBuilder(_token);
            return requestBuilder.Get<IEnumerable<Models.SalesInvoiceBatch>>("SalesInvoiceBatch/GetList");
        }

        public IEnumerable<Models.SalesInvoiceBatch> GetUnprocessedList()
        {
            var requestBuilder = new RequestBuilder(_token);
            return requestBuilder.Get<IEnumerable<Models.SalesInvoiceBatch>>("SalesInvoiceBatch/GetUnprocessedList");
        }
    }
}
