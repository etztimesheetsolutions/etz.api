﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using IdentityModel.Client;

namespace Etz.Api.Client.Utilities
{
    public class RequestBuilder
    {
        private HttpClient HttpClient { get; set; }

        public RequestBuilder(TokenResponse token)
        {
            if (token == null)
                throw new ArgumentException("Token cannot be null.", "Token");

            HttpClient = new HttpClient { BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings["WebServiceBaseAddress"]) };
            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpClient.SetBearerToken(token.AccessToken);
        }

        public void Post<T>(string methodName, T data)
        {
            var response = HttpClient.PostAsJsonAsync(methodName, data).Result;

            if (response.StatusCode != HttpStatusCode.OK)
                ComposeException(response);
        }

        public T Post<T, TU>(string methodName, TU data)
        {
            var response = HttpClient.PostAsJsonAsync(methodName, data).Result;

            if (response.StatusCode != HttpStatusCode.OK)
                ComposeException(response);

            return response.Content.ReadAsAsync<T>().Result;
        }

        public T Get<T>(string methodName)
        {
            return Get<T>(methodName, string.Empty);
        }

        public T Get<T>(string methodName, string requestParams)
        {
            var methodNameAndParams = GetMethodNameAndParams(methodName, requestParams);

            var response = HttpClient.GetAsync(methodNameAndParams).Result;

            if (response.StatusCode != HttpStatusCode.OK)
                ComposeException(response);

            return response.Content.ReadAsAsync<T>().Result;
        }

        private static string GetMethodNameAndParams(string methodName, string requestParams)
        {
            return methodName + (string.IsNullOrEmpty(requestParams) ? string.Empty : "?" + requestParams);
        }

        private static void ComposeException(HttpResponseMessage response)
        {
            throw new ApplicationException(string.Format("Call to API resulted in response code {0} ({1}). Message: {2}", response.StatusCode, (int)response.StatusCode, response.ReasonPhrase));
        }
    }
}
