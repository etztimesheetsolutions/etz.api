﻿using System;

namespace Etz.Api.Client.Utilities
{
    public class File
    {
        public static byte[] ReadFileContentToBytes(string fileName)
        {
            return System.IO.File.ReadAllBytes(fileName);
        }

        public static string ConvertBytesToBase64String(byte[] bytes)
        {
            return Convert.ToBase64String(bytes);
        }
    }
}
