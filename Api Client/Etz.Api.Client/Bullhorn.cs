﻿using IdentityModel.Client;
using Etz.Api.Client.Models;
using Etz.Api.Client.Utilities;

namespace Etz.Api.Client
{
    public class Bullhorn
    {
        private readonly TokenResponse _token;

        public Bullhorn(TokenResponse token)
        {
            _token = token;
        }

        public void BulkUpdatePlacement(int[] placementIds)
        {
            var requestData = new BulkUpdatePlacementRequest { PlacementList = string.Join(",", placementIds) };

            var requestBuilder = new RequestBuilder(_token);
            requestBuilder.Post("Bullhorn/BulkUpdatePlacement", requestData);
        }
    }
}
