﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Etz.Api.Client;

namespace Etz.Api.Sample.Console
{
    public class Program
    {
        private static readonly string WebServiceBaseAddress = ConfigurationManager.AppSettings["WebServiceBaseAddress"];
        private static HttpClient _client;
        private static TokenClient _tokenClient;

        private static string _username = ConfigurationManager.AppSettings["Username"];
        private static string _password = ConfigurationManager.AppSettings["Password"];
        public static void Main(string[] args)
        {
            _client = new HttpClient { BaseAddress = new Uri(WebServiceBaseAddress) };
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _tokenClient = new TokenClient();

            var itemId = new Guid("4BA79BE8-FB83-4EA4-A34A-AB79016E0863");
            System.Console.WriteLine("Example 1 - Call API directly");
            var token = _tokenClient.AuthenticateUser(_username, _password).Result;
            if (!token.IsError)
                _client.SetBearerToken(token?.AccessToken);

            var response = _client.GetAsync("ImportStatus/GetItemStatus?ItemId=" + itemId).Result;

            System.Console.WriteLine(response.IsSuccessStatusCode ? $"request was successful, message {response.Content.ReadAsStringAsync().Result}" : "Request failed");
            System.Console.WriteLine("Example 2 - Use the Client");
            var etzClient = new ImportStatus(token);
            var result =  etzClient.GetItemStatus(itemId);
            System.Console.WriteLine($"Api Client Result, message is {result.Comment}");

            System.Console.ReadLine();
        }

        private static void Setup()
        {
            _client = new HttpClient { BaseAddress = new Uri(WebServiceBaseAddress) };
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _tokenClient = new TokenClient();
            
        }
    }
}
