using System.Collections.Generic;

namespace GetOAuthTokenSample
{
    internal class Auth0User
    {
        public Auth0User(IDictionary<string, string> accountProperties)
        {
            Auth0AccessToken = accountProperties.ContainsKey("access_token") ? accountProperties["access_token"] : string.Empty;
            IdToken = accountProperties.ContainsKey("id_token") ? accountProperties["id_token"] : string.Empty;
        }

        public string Auth0AccessToken { get; set; }
        public string IdToken { get; set; }
    }
}