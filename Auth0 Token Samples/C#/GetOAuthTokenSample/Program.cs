﻿using System;

namespace GetOAuthTokenSample
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            const string username = "username";
            const string password = "password";

            try
            {
                var tokenProvider = new TokenProvider();
                var idToken = tokenProvider.GetToken(username, password);

                Console.WriteLine("Token:\n" + idToken);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:\n" + ex);
            }

            Console.WriteLine("\n\nPress RETURN to exit.");
            Console.ReadLine();
        }
    }
}