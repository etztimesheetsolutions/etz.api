using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;

namespace GetOAuthTokenSample
{
    public class TokenProvider
    {
        public TokenProvider()
        {
            Domain = ConfigurationManager.AppSettings["Auth0Domain"];
            ClientId = ConfigurationManager.AppSettings["Auth0ClientId"];
            DatabaseConnection = ConfigurationManager.AppSettings["Auth0DatabaseConnection"];
            Url = string.Format("https://{0}/oauth/ro", Domain);
        }

        public string Domain { get; private set; }
        public string DatabaseConnection { get; private set; }
        public string ClientId { get; private set; }
        public string Url { get; private set; }

        public string GetToken(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentException("Username is required.", "userName");

            if (string.IsNullOrEmpty(password))
                throw new ArgumentException("Password is required.", "password");


            using (var client = new HttpClient())
            {
                var values = new Dictionary<string, string>
                {
                    {
                        "client_id",
                        ClientId
                    },
                    {
                        "connection",
                        DatabaseConnection
                    },
                    {
                        "username",
                        userName
                    },
                    {
                        "password",
                        password
                    },
                    {
                        "grant_type",
                        "password"
                    },
                    {
                        "scope",
                        "openid"
                    }
                };

                var content = new FormUrlEncodedContent(values);
                var response = client.PostAsync(Url, content).Result;

                if (response.StatusCode != HttpStatusCode.OK)
                    throw new ApplicationException(string.Format("Unexpected response. Code: {0} : Message: {1}", (int) response.StatusCode, response.ReasonPhrase));

                var responseString = response.Content.ReadAsStringAsync().Result;
                var accountProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseString);
                var auth0User = new Auth0User(accountProperties);

                return auth0User.IdToken;
            }
        }
    }
}