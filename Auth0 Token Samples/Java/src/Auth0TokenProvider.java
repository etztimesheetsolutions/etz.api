import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class Auth0TokenProvider {

	public String GetToken() throws Exception {

		String url = "https://etz.auth0.com/oauth/ro";
		String auth0ClientId = "YLDuimFogCF70Zbm39JAGfA6O4oYR5rp";
		String auth0DatabaseConnection = "Username-Password-Authentication";
		String username = "theonlyleo@hotmail.com";
		String password = "leoleo00";

		URIBuilder ub = new URIBuilder(url);
		URI uri = ub.build();

		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("client_id", auth0ClientId));
		urlParameters.add(new BasicNameValuePair("username", username));
		urlParameters.add(new BasicNameValuePair("password", password));
		urlParameters.add(new BasicNameValuePair("connection", auth0DatabaseConnection));
		urlParameters.add(new BasicNameValuePair("grant_type", "password"));
		urlParameters.add(new BasicNameValuePair("scope", "openid"));
		urlParameters.add(new BasicNameValuePair("device", "openid"));

		HttpUriRequest request = RequestBuilder.post().setUri(uri)
				.setHeader(HttpHeaders.ACCEPT, "application/json")
				.setEntity(new UrlEncodedFormEntity(urlParameters)).build();

		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = client.execute(request);

		StatusLine status = response.getStatusLine();

		int statusCode = status.getStatusCode();		
		String reason = status.getReasonPhrase();

		System.out.println("Status code: " + statusCode);		
		System.out.println("Reason: " + reason);		
				
		HttpEntity entity = response.getEntity();
		String responseString = EntityUtils.toString(entity, "UTF-8");

		JSONObject jwt = new JSONObject(responseString);
		String id_token = (String) jwt.get("id_token");
		
		return id_token;
	}
}