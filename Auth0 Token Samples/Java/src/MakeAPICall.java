public class MakeAPICall
{
	public static void main(String[] args)
	{	
		Auth0TokenProvider auth0TokenProvider = new Auth0TokenProvider();
		EtzApi api = new EtzApi();
	
		try {
			System.out.println("Calling GetToken...");		
						
			String id_token = auth0TokenProvider.GetToken();

			System.out.println("Token is: " + id_token);		

			
			System.out.println("Calling GetBatchList...");		
						
			api.GetBatchList(id_token);
			
			System.out.println("Finished!");		
						
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
}
 