import java.net.URI;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class EtzApi {
	
	public String GetBatchList(String token) throws Exception {
			
		String url = "https://api.timesheetz.net/V1/PayrollBatch/GetList";
	
		URIBuilder ub = new URIBuilder(url);
		URI uri = ub.build();
		
		HttpUriRequest request = RequestBuilder.get().setUri(uri)
				.setHeader(HttpHeaders.ACCEPT, "application/json")
				.setHeader("Authorization", "Bearer " + token)
				.build();

		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = client.execute(request);

		StatusLine status = response.getStatusLine();
		int statusCode = status.getStatusCode();		
		String reason = status.getReasonPhrase();

		System.out.println("Status code: " + statusCode);		
		System.out.println("Reason: " + reason);		
		
		HttpEntity entity = response.getEntity();
		String responseString = EntityUtils.toString(entity, "UTF-8");

		return responseString;	
	}	
}
